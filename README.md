## Synopsis

**Notification System**   
This project is used for Notification System.

below feature are supported in this system

1. Accept messages including from, to, subject and body
2. Ability to notify on multiple channels (email, slack)
3. Deliver messages in correct order

## Technologies

1. Spring Boot
2. Maven
3. Swagger

## Installation

Run `maven install` to create application jar.

Go to target folder `cd ./target`

Run `java -jar notification-system--SNAPSHOT.jar` to to start applicatoin.

To get ALL api details `http://localhost:8080/swagger-ui.html#/notification-controller`

## API Details

**API 1: Notify to a Channel**

URL: `http://<HOST>/notifier/{channelType}/notify`

This sends given message to a specified channel like slack or email.
Where the `channelType` is slack or email.

e.g: `http://localhost:8080/notifier/{channelType}/notify`   
with body as:
```javascript 
{  
   "body": "Hi...Thiis is simple test",  
   "from": "erbips@gmail.com",  
   "subject": "Notification Service Test Subject",  
   "to": "tech@gmail.com"  
 }
```

**API 2: Notify All**

URL: `http://<HOST>/notifier/notifyAll`

This sends given message to all configured channels like slack and email.

e.g: `http://localhost:8080/notifier/notifyAll`
with body as: 
```javascript 
{  
   "body": "Hi...Thiis is simple test",  
   "from": "erbips@gmail.com",  
   "subject": "Notification Service Test Subject",  
   "to": "tech@gmail.com"  
 }
```
## Assumption
Here i have not implemented mail service and slack service to send real message only printed the data.
If we want to implement that than we need to add mail jar(already added in pom.xml) and for slack we need to add jar (slack-api-client, do configuraion and application.properties like token and group name)


