package com.here.enums;

public enum ChannelType {
	slack, email;

	@Override
	public String toString() {
		return name().toLowerCase();
	}
}
