package com.here.notification.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.here.dto.MessageDTO;
import com.here.enums.ChannelType;
import com.here.notification.service.NotificationService;

@RestController
@RequestMapping("/notifier")
public class NotificationController {

	@Autowired
	private NotificationService service;

	/**
	 * API is used to notify particular channel.
	 * 
	 * @param channelType
	 * @param msg
	 * @return
	 */
	@PostMapping(value = "/notify/{channelType}")
	public ResponseEntity<String> notify(@PathVariable ChannelType channelType, @RequestBody MessageDTO msg) {
		service.notify(channelType, msg);
		return new ResponseEntity<>("", HttpStatus.OK);
	}

	/**
	 * API is used to notify all channel.
	 * 
	 * @param msg
	 * @return
	 */
	@PostMapping(value = "/notifyAll")
	public ResponseEntity<String> notifyAll(@RequestBody MessageDTO msg) {
		service.notifyAll(msg);
		return new ResponseEntity<>("", HttpStatus.OK);
	}
}
