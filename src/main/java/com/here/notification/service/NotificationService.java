package com.here.notification.service;

import com.here.dto.MessageDTO;
import com.here.enums.ChannelType;

public interface NotificationService {

	public void notifyAll(MessageDTO msg);

	public void notify(ChannelType channelType, MessageDTO msg);

}
