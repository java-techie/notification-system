package com.here.notification.service;

import com.here.dto.MessageDTO;
import com.here.enums.ChannelType;

public interface ChannelService {

	void notify(MessageDTO msg);

	boolean supports(ChannelType type);
}