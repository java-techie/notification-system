package com.here.notification.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.here.dto.MessageDTO;
import com.here.enums.ChannelType;
import com.here.notification.service.ChannelService;

@Service
public class SlackChannelServiceImpl implements ChannelService {

	private static final Logger LOG = LogManager.getLogger(SlackChannelServiceImpl.class);

	@Override
	public void notify(MessageDTO msg) {
		LOG.info("\n Slack Message has been sent \n to : " + msg.getTo() + "\n From : " + msg.getFrom() + " \n Subject"
				+ msg.getSubject() + "\n Msg = " + msg.getBody());
	}

	public boolean supports(ChannelType type) {
		return type.equals(ChannelType.slack);
	}
}
