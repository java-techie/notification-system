package com.here.notification.service.impl;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.here.dto.MessageDTO;
import com.here.enums.ChannelType;
import com.here.notification.factory.ChannelFactory;
import com.here.notification.service.ChannelService;
import com.here.notification.service.NotificationService;

@Service
public class NotificaitonServiceImpl implements NotificationService {
	private static final Logger LOG = LogManager.getLogger(NotificaitonServiceImpl.class);

	@Autowired
	ChannelFactory factory;

	public NotificaitonServiceImpl(ChannelFactory factory) {
		this.factory = factory;
	}

	private AtomicInteger notificationId = new AtomicInteger(1);

	/**
	 * Notifies channel identified by given channelType with the given message.
	 * 
	 * @param msg The message includes from, to, subject, body
	 */
	public void notifyAll(MessageDTO msg) {
		for (ChannelService c : factory.getChannels()) {
			c.notify(msg);
			LOG.debug("ID = " + notificationId + ", Message sent = " + msg);
		}
	}

	public void notify(ChannelType channelType, MessageDTO msg) {
		factory.get(channelType).notify(msg);
		LOG.debug("ID = " + notificationId + ", Message sent = " + msg);

	}
}
