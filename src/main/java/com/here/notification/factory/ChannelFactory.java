package com.here.notification.factory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.here.dto.MessageDTO;
import com.here.enums.ChannelType;
import com.here.notification.service.ChannelService;

@Component
public class ChannelFactory {

	private final List<ChannelService> channelList;

	@Autowired
	public ChannelFactory(List<ChannelService> channelList) {
		this.channelList = channelList;
	}

	public ChannelService get(ChannelType c) {
		return channelList.stream().filter(service -> service.supports(c)).findFirst()
				.orElseThrow(() -> new RuntimeException("No channel found with type : " + c));
	}

	public void notifyAll(MessageDTO msg) {
		for (ChannelService c : channelList) {
			c.notify(msg);
		}
	}

	public List<ChannelService> getChannels() {
		return channelList;
	}
}
